package strategies;

import java.util.LinkedList;

import automail.IMailPool;
import automail.MailItem;

public class MyMailPool implements IMailPool {

	/** Stack of mailItems pending delivery*/
    public LinkedList<MailItem> mailItems;
	
    public MyMailPool(){
        mailItems = new LinkedList<MailItem>();
    }
	
	@Override
	public void addToPool(MailItem mailItem) {
		if (isEmptyPool()) {
			mailItems.addFirst(mailItem);
		} else {
			int i=0;
			for (MailItem m : mailItems) {
				if (getInsertionScore(m) > getInsertionScore(mailItem)) {
					mailItems.add(i, mailItem);
					break;
				}
				i++;
			}
			if (i == mailItems.size()) {
				mailItems.add(mailItem);
			}
		}
	}
	
	private int getInsertionScore(MailItem mailItem) {
		int priorityScore = 0;
		String priority = mailItem.getPriorityLevel();

		switch(priority){
        case "LOW":
        	priorityScore = 3;
            break;
        case "MEDIUM":
        	priorityScore = 2;
            break;
        case "HIGH":
        	priorityScore = 1;
            break;
        }
		
		return mailItem.getSize() * priorityScore;
	}
    
    public boolean isEmptyPool(){
        return mailItems.isEmpty();
    }
    
    public int getSize(){
        return mailItems.size();
    }
    
    public MailItem get(int i){
    	return mailItems.get(i);
    }
    
    public void remove(int i){
        mailItems.remove(i);
    }
}

### What is this repository for? ###
This is an assignment for SWEN30006 Software Modelling and Design, University of Melbourne. (Semester 1, 2017) 


### How do I get set up? ###

1. Extract the zip files’s contents to your desired location
2. Open Eclipse
3. Import a new Eclipse project (File >> Import . . . >> General >> Existing Projects into Workspace)
4. Select the unzipped folder as the directory (and Next as required to complete the import)
5. Run Simulation.java
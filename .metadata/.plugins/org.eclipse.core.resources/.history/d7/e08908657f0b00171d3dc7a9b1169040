package strategies;

import exceptions.TubeFullException;

import automail.MailItem;
import automail.IMailSorter;
import automail.Clock;
import automail.StorageTube;

/**
 * A sample class for sorting mail:  this strategy just takes a MailItem
 * from the MailPool (if there is one) and attempts to add it to the Robot's storageTube.
 * If the MailItem doesn't fit, it will tell the robot to start delivering (return true).
 */
public class MailSorter implements IMailSorter{

	MailPool MailPool;
	
	public MailSorter(MailPool MailPool) {
		this.MailPool = MailPool;
	}
    /**
     * Fills the storage tube
     */
    @Override
    public boolean fillStorageTube(StorageTube tube) {
    	int currCapacity;
    	// System.out.println("MailPool.mailItems.count: "+MailPool.mailItems.size());
        try{
        	if (tube.isEmpty()) {
        		currCapacity = tube.MAXIMUM_CAPACITY;
        	}
            if (!MailPool.isEmptyPool()) {
	            boolean keepLooking = true;
	            
	            while (keepLooking) {
	            	MailItem m = MailPool.get();
	            	if (m.getSize() <= currCapacity) {
	            		tube.addItem(m);
	            		MailPool.remove(m);
	            	}
	            }
	            tube.addItem();
	            /** Remove the item from the ArrayList */
	            MailPool.remove();
            }
        }
        /** Refer to TubeFullException.java --
         *  Usage below illustrates need to handle this exception. However you should
         *  structure your code to avoid the need to catch this exception for normal operation
         */
        catch(TubeFullException e){
        	return true;
        }      
        /** 
         * Handles the case where the last delivery time has elapsed and there are no more
         * items to deliver.
         */
        if(Clock.Time() > Clock.LAST_DELIVERY_TIME && MailPool.isEmptyPool() && !tube.isEmpty()){
            return true;
        }
        return false;

    }
}
